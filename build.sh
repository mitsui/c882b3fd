#!/bin/bash
set -e
export HOME=`pwd`

TARGETS=${TARGETS:-"linux/amd64"}
VERSION=${VERSION:-"dev-next"}
GEOIPS=${GEOIPS:-"cn"}
GEOSITES=${GEOSITES:-"category-ads-all cn geolocation-cn"}
BUILD_LDFLAGS=${BUILD_LDFLAGS:-"-s -w -buildid="}
BINARY_DIR=$HOME/binary
MARCHAPP_DIR=$HOME/marchbin
SINGBOX_SERVERTAGS='with_quic,with_wireguard,with_reality_server,with_gvisor'
SINGBOX_CLIENTTAGS='with_quic,with_wireguard,with_ech,with_utls,with_reality_server,with_gvisor'
AMD64_MARCHES="v1 v2 v3 v4"
ARM_MARCHES="7"

function goBuild() {
  #GOOS=windows GOARCH=amd64 GOAMD64=v2 goBuild output_fimename source_path build_tags
  BUILD_SRCFILENAME=${2:-'.'}
  BUILD_BINFILEEXTNAME=''
  GOARCH=${GOARCH:-'amd64'}
  GOOS=${GOOS:-'linux'}
  BUILD_CPUARCHNAME=$GOARCH
  if [ "x$3" = "x" ]; then
    BUILD_TAGOPT=""
  else
    BUILD_TAGOPT="-tags $3"
  fi
  if [ "$GOARCH" = "amd64" ]; then
    GOAMD64=${GOAMD64:-'v1'}
    BUILD_CPUARCHNAME='x86_64-'$GOAMD64
  fi
  if [ "$GOARCH" = "arm" ]; then
    GOARM=${GOARM:-'5'}
    BUILD_CPUARCHNAME='armv'$GOARM
  fi
  if [ "$GOARCH" = "386" ]; then
    BUILD_CPUARCHNAME='x86'
  fi
  if [ "$GOOS" = "windows" ]; then
    BUILD_BINFILEEXTNAME='.exe'
  fi
  echo "build ${1##*/} for $GOOS on $BUILD_CPUARCHNAME"
  go build -trimpath -ldflags="$BUILD_LDFLAGS" -o $1-$GOOS-$BUILD_CPUARCHNAME$BUILD_BINFILEEXTNAME $BUILD_TAGOPT $BUILD_SRCFILENAME
}

echo "Building marchbin"
mkdir $MARCHAPP_DIR
cd gosrc
mkdir temp
GOARCH=amd64 GOAMD64=v1 goBuild temp/archtest .
GOARCH=arm GOARM=5 goBuild temp/archtest .
gzip -n -9 temp/*
mv temp/*.gz $MARCHAPP_DIR/

cd $HOME
mkdir -p $BINARY_DIR
echo 'Fetching sources files...'
git clone --single-branch -b $VERSION https://github.com/SagerNet/sing-box.git
cd sing-box

BUILD_VERSION_TAG=$(git describe --tags | sed -e 's/-[0-9]\+-g\([0-9a-f]\+\)$/+\1/g')
BUILD_START_TIME=$(date -u +'%Y-%m-%d %H:%M:%S UTC')

echo "Current sing-box build version: $BUILD_VERSION_TAG"

echo 'Prepare all dependencies...'
go get -t -d ./...
cd cmd/sing-box

echo 'Start building...'
SINGBOX_BUILD_CLIENT_COMMAND="goBuild $BINARY_DIR/sing-box-client . $SINGBOX_CLIENTTAGS"
SINGBOX_BUILD_SERVER_COMMAND="goBuild $BINARY_DIR/sing-box-server . $SINGBOX_SERVERTAGS"
SINGBOX_LDFLAGS="-X 'github.com/sagernet/sing-box/constant.Version=$BUILD_VERSION_TAG' $BUILD_LDFLAGS"
for t in $TARGETS; do
  if [ "${t%/*}" = "windows" ]; then
    nameext='.exe'
  else
    nameext=''
  fi
  if [ "${t#*/}" = "amd64" ]; then
    for ma in $AMD64_MARCHES; do
      GOOS=${t%/*} GOARCH=amd64 GOAMD64=$ma BUILD_LDFLAGS="$SINGBOX_LDFLAGS" $SINGBOX_BUILD_CLIENT_COMMAND
      GOOS=${t%/*} GOARCH=amd64 GOAMD64=$ma BUILD_LDFLAGS="$SINGBOX_LDFLAGS" $SINGBOX_BUILD_SERVER_COMMAND
    done
  elif [ "${t#*/}" = "arm" ]; then
    for ma in $ARM_MARCHES; do
      GOOS=${t%/*} GOARCH=arm GOARM=$ma BUILD_LDFLAGS="$SINGBOX_LDFLAGS" $SINGBOX_BUILD_CLIENT_COMMAND
      GOOS=${t%/*} GOARCH=arm GOARM=$ma BUILD_LDFLAGS="$SINGBOX_LDFLAGS" $SINGBOX_BUILD_SERVER_COMMAND
    done
  else
    GOOS=${t%/*} GOARCH=${t#*/} BUILD_LDFLAGS="$SINGBOX_LDFLAGS" $SINGBOX_BUILD_CLIENT_COMMAND
    GOOS=${t%/*} GOARCH=${t#*/} BUILD_LDFLAGS="$SINGBOX_LDFLAGS" $SINGBOX_BUILD_SERVER_COMMAND
  fi
done

gzip -n -9 $BINARY_DIR/*

echo "Downloading geosite and geoip data..."
mkdir $HOME/geodata
cd $HOME/geodata
for t in $GEOIPS; do
  curl -Lo "geoip-$t.srs" "https://raw.githubusercontent.com/SagerNet/sing-geoip/rule-set/geoip-$t.srs"
done
for t in $GEOSITES; do
  curl -Lo "geosite-$t.srs" "https://raw.githubusercontent.com/SagerNet/sing-geosite/rule-set/geosite-$t.srs"
done
tar --numeric-owner -c -z -f $HOME/geodata.tar.gz *

sed -i 's|DOWNLOAD_BASE_URL=|\0"'$CI_JOB_URL/artifacts/raw'"|' $HOME/scripts/install.sh
sed -i "s|{BUILD_INFO}|$BUILD_VERSION_TAG @ $BUILD_START_TIME|" $HOME/index.html
sed -i "s|{SCRIPT_URL_ROOT}|$CI_JOB_URL/artifacts/raw/scripts|" $HOME/index.html

cd $BINARY_DIR
if [ "$(ls -A .)" ]; then
  echo 'Build finished, waiting for artifacts storing process...'
  exit 0
fi

echo 'No build found at release directory!'
exit 1
