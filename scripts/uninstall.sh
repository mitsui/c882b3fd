#!/bin/sh
systemctl disable sbsd || true
systemctl stop sbsd || true
rm -f /usr/local/bin/sbsd
rm -f /etc/systemd/system/sbsd.service
rm -rf /etc/sbsd
