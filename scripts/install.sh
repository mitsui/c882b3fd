#!/bin/bash

set -e

DOWNLOAD_BASE_URL=

if type curl > /dev/null 2>&1; then
  DOWNLOADER='curl -sL'
elif type wget > /dev/null 2>&1; then
  DOWNLOADER='wget -qO-'
else
  echo -e "\e[31mCan not download file: curl/wget not found\e[0m" >&2
  exit 1
fi

install_service=yes
service_file=/etc/systemd/system/sbsd.service

while [ $# -gt 0 ]; do
  if [ "$1" = "-S" ]; then
    install_service=no
  fi
  shift 1
done

fetchFile() {
  if [ "x$2" = "x" ]; then
    $DOWNLOADER $1
  else
    $DOWNLOADER $1 | gzip -c -d - >$2
  fi
}

test -e /etc/sbsd || mkdir -p /etc/sbsd
arch=`uname -m`
case "$arch" in
    i?86) arch=x86 ;;
    x86_64) arch=x86_64-v1 ;;
    armv6l) arch=armv6 ;;
    armv7l) arch=armv7 ;;
    armv8l) arch=arm64 ;;
    aarch64) arch=arm64 ;;
esac
work_dir=/tmp/$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c16)
mkdir -p $work_dir
cd $work_dir

if [ "$arch" = "x86_64-v1" ]; then
  fetchFile $DOWNLOAD_BASE_URL/marchbin/archtest-linux-x86_64-v1.gz ./archtest
  chmod +x ./archtest
  amd64ver=$(./archtest)
  if [ "$amd64ver" = "2" ]; then
    arch=x86_64-v2
  elif [ "$amd64ver" = "3" ]; then
    arch=x86_64-v3
  elif [ "$amd64ver" = "4" ]; then
    arch=x86_64-v4
  fi
fi

echo -e "\e[36mCurrent platform architecture is $arch\e[0m"

echo -e '\e[36mDownloading sing-box binary...\e[0m'
fetchFile "$DOWNLOAD_BASE_URL/binary/sing-box-server-linux-$arch.gz" ./sbsd
install ./sbsd /usr/local/bin/
echo -e "\e[36mDownloading and installing Geo database\e[0m"
fetchFile $DOWNLOAD_BASE_URL/geodata.tar.gz | tar xz
cp -f *.srs /etc/sbsd

if [ "$install_service" = "yes" ]; then
  echo -e "\e[36mInstalling service...\e[0m"
  cat << EOF > $service_file
[Unit]
Description=Sing-Box Server Daemon
After=network-online.target
Wants=network-online.target

[Service]
User=nobody
ExecStart=/usr/local/bin/sbsd run
Restart=on-failure
RestartSec=8s
WorkingDirectory=/etc/sbsd
ExecReload=/bin/kill -HUP \$MAINPID
CapabilityBoundingSet=CAP_NET_ADMIN CAP_NET_BIND_SERVICE CAP_NET_RAW CAP_DAC_READ_SEARCH
AmbientCapabilities=CAP_NET_ADMIN CAP_NET_BIND_SERVICE CAP_NET_RAW CAP_DAC_READ_SEARCH
LimitNOFILE=infinity
DeviceAllow=/dev/null rw
DeviceAllow=/dev/net/tun rw

[Install]
WantedBy=multi-user.target
EOF
  chmod 0644 $service_file
  systemctl daemon-reload || true
fi

if [ ! -e /etc/sbsd/config.json ]; then
  echo -e "\e[32mYou may need creating your configs\e[0m"
fi

if [ ! -e /etc/sbsd/cert.pem ]; then
  echo -e "\e[36mGenerating a self-signed certificate for\e[0m"
  openssl ecparam -genkey -name prime256v1 -out /etc/sbsd/key.pem
  openssl req -key /etc/sbsd/key.pem -x509 -days 1826 -subj "/C=US/ST=Utah/L=Orem/O=V2/OU=Org/CN=*" -out /etc/sbsd/cert.pem
  chmod 0644 /etc/sbsd/key.pem
fi

rm -rf $work_dir

echo -e "\e[32mUse 'systemctl start sbsd' to start service.\e[0m"
